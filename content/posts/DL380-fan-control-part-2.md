---
title: "DL380 Fan Control Part 2"
slug: dl380-fan-control-part-2
description: null
date: 2020-05-28T20:25:03+02:00
type: posts
draft: false
categories:
- General
tags:
- server
- fancontrol
- hp
- DL380e
series:
- My DL380e G8
---

My server has been in use for a few months, and has been performing admirably, running the usual self-hosted suspects; NextCloud, PleX, Unifi etc, as well as facilitating my thesis for my masters in educational sociology.

This is going great — but there was one nagging annoyance: the fan curve had a bit of hysteresis in it that ramped the fans up; then down; then up;then down.
My server closet is pretty much in the middle of the house as well, so noise becomes annoying fast.
And, it is a literal closet; so the thermal solution isn't great.
Oh, to have a basement or outhouse...


There is a fix, though: Changing the hysteresis profile associated with thermal sensor 1 in the DL380e.

this is achieved by running

```
fan t 0 hyst 2
```

from the iLO prompt.

Hysteresis profile 2 was selected on the basis of being the profile of all other sensors, except the PSU sensors.
The front ambient sensors ran hysteresis profile 3 previously.

# The results

Fan utilization is now at a consistent 9.4-20-13-16-20-16 percent, which is decently quiet.
The tradeoff, however, is higher component and ambient temperatures (all values in Celsius, at mostly idle):

```
Sensor      Now     Before
==========================
Ambient     28-33   26-32
Exhaust     47-50   44-46
P420        70-75   64-66
CPU         50-55   40-45
Disks       44-52   42-47
```

This, I'm willing to accept.
YMMV.
It is, however, *imperative* to actively cool the P420 - that's a HOT chip.


Now, I want to investigate a R210ii fan swap. The higher ambient temps brought on by summer and, y'know, using this old thing, has set those fans (slightly) on fire.
Those 40mm fans make their presence known!

# More to play with

You can change the threshold values with:

```
fan t <n> caut <offset from original value>
fan t <n> crit <offset from original caut>
```

But I haven't had a chance or a need to mess around.
There is also a lot of tunables per sensor regarding setpoints, gain values and somesuch.
I get rather lost, rather quickly.
