---
title: "Add a GPU to the DL380e"
slug: dl380-gpu
description: Tips and tricks for adding a GPU to a DL380e G8
date: 2020-04-16T12:08:28+02:00
type: posts
draft: false
categories:
- Home Computing
tags:
- GPU
- nvidia-patch
- transcode
- linux
series:
- My DL380e G8
---
# Because the CPU is for, uh, my thesis!

After observing PLeX streams churning up some CPU time, I decided to get a GPU to offload transcoding tasks.
I went with a GTX 1050 Ti 4GB.
This is more or less the same chip as the Quadro P2000, which is the current ~budget~, uh, low-powered darling.
With [a sneaky workaround](https://github.com/keylase/nvidia-patch), the artificial two-transcode limit is easily circumvented.
For 5 times less than a P2000, I'll take that deal!

# GPU installation patch
To pass your GPU through to your PLeX docker, there are preparatory steps needed.

## 0. Install Community Applications

## 1. Install unRAID-nVidia

From Community Applications install unRAID-nVidia.
Go to Settings → unRAID-nVidia, Select the nVidia build for your version of unRAID, and install.

## 2. Prepare patch

```sh
su
cd /boot
wget https://raw.githubusercontent.com/keylase/nvidia-patch/master/patch.sh
chmod +x patch.sh
mv patch.sh nvidia-patch.sh
cat /boot/nvidia-patch.sh >> config.go
```

## 3. Power down, and install GPU

Installing the card is straightforward.
Remove the PCIe riser, insert the card in the 16x wide slot, reinsert, reboot.

![The GPU fits!](/img/dl380e_1050ti.jpeg)

Do note that the 16x slot is 8x electrical; but this does not matter for our purposes.
My particular card did not need an extra GPU power cable.
If yours does, you need the 10 pin to GPU power adapter from HP, or get [this one from moddiy.com](https://www.moddiy.com/products/HP-Server-DL380-Gen8-10-Pin-to-8-Pin-and-6-Pin-GPU-PCIE-Power-Cable.html)

## 4. Reboot, and configure the PLeX docker

...but first, go to Settings → unRAID-nVidia; and copy your GPU GUID somewhere convenient.

In the unRAID web UI navigate to Docker, and reconfigure the PLeX docker.
Switch to advanced view, and under "Extra Parameters" add `--runtime=nvidia`.
Under NVIDIA_VISIBLE_DEVICES add that GUID.
Save, restarting the PLeX docker.

## 5. Enable hardware transcoding in PLeX

In the PLeX webui, go to settings for your server.
Under "Transcoding", select "Use hardware transcoding when available"

And there you go!

# Pipe down, you!

Remember the fan control rain dance from the last entry in this series?
And HPE's agressive stance towards fan control?
Well, HPE's not gonna let you forget.
After installing the GPU, my fans were running at an... excessive > 60 %.
And my previous fan hack - just setting every fan baseline to 1 - didn't work anymore.
[This reddit post](https://www.reddit.com/r/homelab/comments/di3vrk/silence_of_the_fans_controlling_hp_server_fans/firx6op?utm_source=share&utm_medium=web2x)
could point me in the right direction, though.

This process is also a little involved, so buckle up.

## 1. Reset iLO.

There's a bug in the fan-control hacked firmware that makes it not display command outputs in SSH sessions beyond the first after a reset.
And this output is important for the next step.

## 2. Figure out which sensor is making iLO freak out

iLO is, as my son so eloquently put it, crying "stranger danger" on account of not recognizing the GPU.
This can be illustrated by SSHing into iLO, and running the command `fan info g`.

A nice table like the following should be presented:

```txt
GROUPINGS
0: FASTEST Output:  63  [02*07 ...
1: FASTEST Output:  63  [02*07 ...
2: FASTEST Output:  35  [01 02*...
3: FASTEST Output:  36  [01 02 ...
4: FASTEST Output:  60  [01 03 ...
5: FASTEST Output:  60  [01 05 ...
```

(Example borrowed from the linked Reddit post, since I forgot to save my actual output)

Note that some numbers are marked with an `*`.
This indicates that that is the sensor iLO is reading as the hottest - in my case, sensor 52.

## I said, be QUIET!

To quiet down just that sensor, run `fan pid 52 hi 300` or some other low number.
And enjoy immediate relief, as your fans settle down somewhere around 10-15 %.

# Results

Quick testing yielded 2 4K → 1080p transcodes, at ~1500 megabytes of GPU ram each; alongside one 1080p → 720p transcode.
Realistically, I wont have much more than one 4K transcode at any given moment, if at all.
Very nearly 0 CPU usage though, which was nice.

Feels good when a plan comes together.
