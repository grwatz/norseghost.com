---
title: "Unraid Persistent Home"
slug: unraid-persistent-home
description: null
date: 2020-04-22T01:03:08+02:00
type: posts
draft: false
categories:
- unRAID
tags:
- unraid
- hacks
series:
-
---

In [the unRAID subreddit](https://reddit.com/r/unraid) I came across someone needing to set up a ssh key for accessing files remotely.

There is, however, a step zero to perform before this can work.

# The problem

When unRAID boots, most of the filesystem is copied from the boot USB into RAM.
This means that any changes will be lost upon a reboot.
Thus, simply doing `mkdir /home/norseghost` for user settings won't work.

Persistent home is still possible, though.

# The solution

Create a home directory somewhere on the array. My location is `/mnt/user/system/home`. I’ve set the share *system* to be cache-prefer.

Install the **User Scripts** plugin. Create a new script - let’s call it `mount_home`:

```sh
#!/bin/bash

mount -o bind /mnt/user/system/home /home
```

This [bind-mounts](https://unix.stackexchange.com/questions/198590/what-is-a-bind-mount) the newly created home dir on to system `/home`.
Set it to run on array start. And run it now, while you’re at it.

Create another one - `unmount_home`:

```sh
#!/bin/bash

umount  /home
```

Set that one to run on array stop. This is to enable array shutdown without “device busy” errors.

# Profit!

Now unRAID has a persistent home.
Create a home directory for any user that would log in remotely:

```
root@unraid# mkdir /home/norseghost
chown norseghost:norseghost /home/norseghost.
```

And now you can add passwordless ssh login, shell profile customizations, or whatever.
